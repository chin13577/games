﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{

    public Weapon weapon;
    // set detail to weapon from inspector
    public int id;
    public string weaponName;
    public Item.ItemType itemType;
    public Weapon.WeaponType weaponType;
    public Weapon.HandedType handedType;
    public Weapon.Size size;
    public int price;
    public int weight;
    public int atk;
    public string detail;
    [Header("Buffs status")]
    public int strength;
    public int perception;
    public int endurance;
    public int charisma;
    public int intelligence;
    public int agility;
    public int luck;
    // Use this for initialization
    void Start()
    {
        weapon = new Weapon(id, weaponName, itemType, price, weight);
        weapon.addValueToWeapon(atk, weaponType, handedType, size, strength, perception, endurance, charisma, intelligence, agility, luck, detail);
    }
    
}
