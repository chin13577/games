﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public enum ItemType { Weapon, Armor, Accessory, Potion };
    public ItemType itemType;
    public int id;
    public string name;
    public int price;
    public int weight;

}
