﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item
{
    public enum WeaponType { Sword, Hammer, Axe, Bow, Staff, Dagger };
    public enum HandedType { OneHanded, TwoHanded };
    public enum Size { Small, Medium, Large };
    public WeaponType weaponType;
    public HandedType handedType;
    public Size size;
    public int atk;
    public string detail;
    [Header("Buffs")]
    public int buffStrength;
    public int buffPerception;
    public int buffEndurance;
    public int buffCharisma;
    public int buffIntelligence;
    public int buffAgility;
    public int buffLuck;

    public Weapon(int id, string name, ItemType type, int price, int weight)
    {
        base.id = id;
        base.name = name;
        base.itemType = type;
        base.price = price;
        base.weight = weight;
    }
    public void addValueToWeapon(int atk, WeaponType weaponType, HandedType handedType, Size size, int strength, int perception, int endurance, int charisma, int intelligence,
                                 int agility, int luck, string detail)
    {
        this.atk = atk;
        this.weaponType = weaponType;
        this.handedType = handedType;
        this.size = size;
        this.buffStrength = strength;
        this.buffPerception = perception;
        this.buffEndurance = endurance;
        this.buffCharisma = charisma;
        this.buffIntelligence = intelligence;
        this.buffAgility = agility;
        this.buffLuck = luck;
        this.detail = detail;
    }
}
