﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Status
{
    //Main Attributes
    public int strength;
    public int perception;
    public int endurance;
    public int charisma;
    public int intelligence;
    public int agility;
    public int luck;
    //Combat
    public int atk;
    public int def;
    public int hp;
    public int mp;
    //Others
    public enum Race { Human, Elf };
    public enum Gender { Male, Female };
    public string playerName;
    public Race race;
    public Gender gender;
    public int carryWeight;
    public int equipWeight;
    public int gold;
    


}
