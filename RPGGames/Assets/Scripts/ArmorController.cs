﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmorController : MonoBehaviour {

    public Armor armor;
    // set detail to armor from inspector
    public int id;
    public string armorName;
    public Item.ItemType itemType;
    public Armor.ArmorType armorType;
    public Armor.Size size;
    public int price;
    public int weight;
    public int def;
    public string detail;
    [Header("Buffs status")]
    public int strength;
    public int perception;
    public int endurance;
    public int charisma;
    public int intelligence;
    public int agility;
    public int luck;

    // Use this for initialization
    void Start () {
        armor = new Armor(id, armorName, itemType, price, weight);
        armor.addValueToArmor(def, armorType, size, strength, perception, endurance, charisma, intelligence, agility, luck, detail);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
