﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : Item
{
    public enum ArmorType { Head,Hand,Foot,Body,Face };
    public enum Size { Small, Medium, Large };
    public ArmorType armorType;
    public Size size;
    public int defense;
    public string detail;
    public int buffStrength;
    public int buffPerception;
    public int buffEndurance;
    public int buffCharisma;
    public int buffIntelligence;
    public int buffAgility;
    public int buffLuck;

    public Armor(int id, string name, ItemType type, int price, int weight)
    {
        base.id = id;
        base.name = name;
        base.itemType = type;
        base.price = price;
        base.weight = weight;
    }
    public void addValueToArmor(int def, ArmorType armorType, Size size, int strength, int perception, int endurance, int charisma, int intelligence,
                                 int agility, int luck, string detail)
    {
        this.defense = def;
        this.armorType = armorType;
        this.size = size;
        this.buffStrength = strength;
        this.buffPerception = perception;
        this.buffEndurance = endurance;
        this.buffCharisma = charisma;
        this.buffIntelligence = intelligence;
        this.buffAgility = agility;
        this.buffLuck = luck;
        this.detail = detail;
    }
}
